FROM python:3.7
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip3 install --no-cache-dir --default-timeout=100 -r requirements.txt
COPY . .
WORKDIR autoctovin
CMD ["scrapy", "crawl", "autoctovin"]