from urllib.parse import urlparse
from pathlib import Path
from functools import partial
import re
from datetime import datetime, timedelta
import json

from cleantext import clean as clean_
from bs4 import BeautifulSoup
from scrapy import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule, CrawlSpider
import environ

from ..items import AutoctovinItem

BASE_DIR = Path.cwd().parent
env = environ.Env()
environ.Env.read_env("../.env")

clean = partial(clean_, no_currency_symbols=True, no_punct=True)


class AutoctovinSpider(CrawlSpider):
    name = "autoctovin"
    # own
    from_file = env.bool("FROM_FILE")
    limit_per_site = timedelta(seconds=10)
    get_chars_around_word = env.int("GET_CHARS_AROUND_WORD")

    def __init__(self, offset=None, *a, **kw):
        self.slice_ = None
        offset = offset or env.str("OFFSET")
        if offset:
            start, stop = offset.split("-")
            start = int(start) if start else None
            stop = int(stop) if stop else None
            self.slice_ = slice(start, stop)

        with open(BASE_DIR.joinpath("grapes_list.txt")) as file:
            self.grape_regex_list = []
            for line in file.readlines():
                clean_grape = clean(line)
                self.grape_regex_list.append(
                    (
                        clean_grape,
                        re.compile(
                            fr"\b{clean_grape}\b", flags=re.MULTILINE | re.IGNORECASE
                        ),
                    )
                )
        super().__init__(*a, **kw)

    def start_requests(self):
        if self.from_file:
            with open(BASE_DIR.joinpath("sites_list.txt")) as file:
                for url in (self.format_url(url) for url in file):
                    yield Request(url, callback=self.parse, dont_filter=True)
        else:
            with open(BASE_DIR.joinpath("input_data.json")) as file:
                json_data = json.load(file)
                self.log(f"total length: {len(json_data)}")
                if self.slice_:
                    json_data = json_data[self.slice_]
                self.log(f"sliced length: {len(json_data)}")

                for entry in json_data:
                    raw_link = entry.get("website")
                    if not raw_link:
                        continue
                    url = BeautifulSoup(raw_link).find("a", href=True)["href"]
                    # for url in (self.format_url(url) for url in file):
                    yield Request(url, callback=self.parse, dont_filter=True)

    @staticmethod
    def format_url(url):
        if not re.match(r"(?:http|https)://", url):
            return f"http://{url}"
        return url

    def parse(self, response):
        """
        Run for each unique site
        """
        started_at = datetime.now()
        # TODO: parse root page
        hostname = urlparse(response.url).hostname
        if hostname.startswith("www."):
            hostname = hostname[4:]
        self.log(f"UNIQ: {hostname}")
        deny_words_in_url = env.list("DENY_WORDS_IN_URL", default=[])
        deny_domains = ("facebook.com", "pinterest.com", "google.com", "twitter.com")
        rule = Rule(
            LinkExtractor(
                deny=[
                    re.compile(fr"[-_=?/]{x}[-_=?/.]", re.MULTILINE | re.IGNORECASE)
                    for x in deny_words_in_url
                ],
                canonicalize=True,
                unique=True,
                allow_domains=hostname,
                deny_domains=deny_domains,
                # restrict_text=("cart",),
            ),
            follow=True,
            callback="parse_items",
            cb_kwargs={"started_at": started_at},
        )
        rule._compile(self)
        self._rules = [rule]
        # Reinit full grapes list for each site
        # self.grapes_to_search = set(self.grapes_list)
        return super().parse(response)

    def parse_items(self, response, started_at):
        """
        Run for each page
        """
        items = []
        text = " ".join(BeautifulSoup(response.body, "html.parser").stripped_strings)
        clean_text = clean(text)
        words_found = self.find_words(clean_text)
        if words_found:
            # remove this words from list, to not search anymore
            # self.grapes_to_search -= words_found
            for word, wrapped_word in words_found:
                item = AutoctovinItem()
                item["hostname"] = urlparse(response.url).hostname
                item["url"] = {"url": response.url, "wrapped": wrapped_word}
                item["word"] = word
                items.append(item)
        return items

    def find_words(self, text) -> set:
        words_found = set()
        for word, regex in self.grape_regex_list:
            match = next(regex.finditer(text), None)
            if match:
                left_offset, right_offset = match.span()

                left_offset -= self.get_chars_around_word
                if left_offset < 0:
                    left_offset = 0

                right_offset += self.get_chars_around_word
                if right_offset > len(text):
                    right_offset = len(text)

                # if regex.findall(text):
                words_found.add((word, text[left_offset:right_offset]))
        return words_found

    # def _parse_response(self, response, callback, cb_kwargs, follow=True):
    #     hostname = cb_kwargs.get("hostname")
    #     started_at = cb_kwargs.get("started_at")
    #     if hostname and started_at:  # TODO debug this
    #         left = datetime.now() - started_at
    #         print("time left ", left)
    #         if started_at + self.limit_per_site < datetime.now():
    #             raise StopIteration
    #     yield from super()._parse_response(response, callback, cb_kwargs, follow)
