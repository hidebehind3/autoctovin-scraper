# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class AutoctovinItem(scrapy.Item):
    # The destination URL
    hostname = scrapy.Field()
    url = scrapy.Field()
    word = scrapy.Field()
