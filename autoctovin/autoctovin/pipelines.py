import pymongo

import environ

env = environ.Env()
environ.Env.read_env("../.env")


class MongoPipeline:
    def __init__(self):
        connection = pymongo.MongoClient(
            "mongo", 27017, username="root", password="example"
        )
        db = connection[env.str("DB_NAME")]
        self.collection = db["sites"]

    def process_item(self, item, spider):
        hostname = item["hostname"]
        url = item["url"]
        word = item["word"]

        if not self.collection.find_one({"_id": hostname, "words.word": word}):
            self.collection.update_one(
                {"_id": hostname}, {"$addToSet": {"words": {"word": word}}}, upsert=True
            )

        self.collection.find_one_and_update(
            {"_id": hostname, "words.word": word},
            {"$addToSet": {"words.$.urls": url}},
            upsert=False,
        )

        return item
