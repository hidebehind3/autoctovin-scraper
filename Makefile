#! /usr/bin/make -f

PROJECT=autoctovin-scraper


run:
	docker-compose up --build --force-recreate --remove-orphans

stop:
	docker-compose stop

scrape:
	cd autoctovin && scrapy crawl autoctovin -a offset=$(offset)

black:
	black autoctovin/
